import './../css/app.css';
import 'font-awesome/css/font-awesome.css';
import $ from "jquery";
import lozad from 'lozad';

import {
    toursData,
    filteredTours,
    fetchData,
    setMinMaxPrices,
    getMaxDiscount,
    filterDates,
    sortTours,
    getStartEndDestination,
    drawAvailabilities,
    hasTourSpacesLeft,
    drawStars
} from './helpers';


/**
 * Renders a tour block.
 * @function
 */
const draw = _ => {
    const tours = filteredTours || toursData;
    let minPrice, maxPrice, dates, primaryImgArr, primaryImgUrl, stars, table = '';
    for (let i = 0; i < tours.length; i++) {
        // only tours with available dates are shown;
        if (hasTourSpacesLeft(tours[i].dates)) {
            minPrice = tours[i].dates.length > 0 ? tours[i].minPrice : 0;
            maxPrice = tours[i].dates.length > 0 ? tours[i].maxPrice : 0;
            stars = drawStars(tours[i].rating);
            primaryImgArr = tours[i].images.filter(image => image.is_primary);
            primaryImgUrl = primaryImgArr.length > 0 ?
                primaryImgArr[0].url || 'img/tr_logo.png' :
                tours[i].images.length > 0 ?
                tours[i].images[0].url :
                'img/tr_logo.png'; // fallback img is company logo

            dates = tours[i].dates.map(date => date.start);
            table += '<div class="tour-container">';

            // Image block
            table += `<div class='tour-img'>
                        <img class="lozad" data-src='${primaryImgUrl}'>
                    </div>`;

            //Info block
            table += `<div class='tour-infos'>
                                    ${getMaxDiscount(tours[i].dates)}
                                    <div class='tour-infos__title'>
                                    <div class='tour-infos__name'>${tours[i].name}</div>
                                    <div class='tour-infos__stars'>${stars}</div>
                                    <div class='tour-infos__reviews'>Reviews ${tours[i].reviews || 0}</div>
                                    <div class='tour-infos__description'>${tours[i].description}</div>
                                    <div class='tour-infos__details'>
                                    <dl>
                                        <dt>Destinations</dt>
                                        <dd>${tours[i].cities.length} destinations</dd>
                                        <dt>Starts/ Ends in</dt>
                                        <dd>${getStartEndDestination(tours[i].cities)}</dd>
                                        <dt>Operator</dt>
                                        <dd>${tours[i].operator_name}</dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>`;

            //Price block
            table += `<div class='tour-offer'>
                                <div class='tour-offer__container'>
                                    <dl class='tour-offer__days'>
                                        <dt>Days</dt>
                                        <dd>${tours[i].length}</dd>
                                    </dl>
                                    <dl class='tour-offer__from'>
                                        <dt>From</dt>
                                        <dd>€ ${tours[i].minPrice}</dd>
                                    </dl>
                                </div>
                                <div class='tour-offer__spaces'>
                                     ${drawAvailabilities(tours[i].dates)}
                                </div>
                                <div class='tour-offer__button' data-expanded=false>
                                     View More
                                </div>
                              </div>`;


            table += '</div>';

        }
    }
    $('#tours-container')
        .html(table);

    toggleLoader(false);

}
/**
 * Scrolls the page to the top.
 * @function
 */
const scrollToTop = _ => {
    $("html, body")
        .animate({
            scrollTop: 0
        }, "slow");
}
/**
 * Inizialisation of img lazy loading.
 * @function
 */
const attachLazyLoad = _ => {
    const observer = lozad();
    observer.observe();
}

/**
 * Toggles the loader.
 * @oarams {boolean} visible
 * @function
 */
const toggleLoader = visible => {
    $('.overlay').toggle(visible)
};

/**
 * Application init.
 * @function
 */
fetchData().then(() => {
    setMinMaxPrices();
    sortTours($('#tours-sorter').val());
    draw();
    attachLazyLoad();
});

/**
 * Filters the paginated tours' dates by the selected date onward.
 * @function
 */
$('#tours-filter').on('change', function() {
    filterDates(this.value);
    sortTours($('#tours-sorter').val()); // keeps the sorting
    draw();
    attachLazyLoad();
    scrollToTop();

});
/**
 * Sort the paginated tours by the selected criteria.
 */
$('#tours-sorter').on('change', function() {
    sortTours(this.value);
    draw();
    attachLazyLoad();
    scrollToTop();
});
/**
 * Toggles the box containeing the available spaces.
 */
$('.content').on('click', '.tour-offer__button', function() {
    if (!$(this).data('expanded')) {
        $(this)
            .siblings('.tour-offer__spaces')
            .css('max-height', '100%');
        $(this)
            .text('View less')
            .data('expanded', true);
    } else {
        $(this)
            .siblings('.tour-offer__spaces')
            .css('max-height', '28px');
        $(this)
            .text('View more')
            .data('expanded', false);
    }

});
