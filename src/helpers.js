import moment from 'moment';

/**
* Variables to store the original sata as fetched from the API,
* and a second variable to handle the data sorted / filtered.
* This is only for the purpose of the demo, in a realistic scenario,
* the endpoint should accept filter and sorting criteria as params as well.
*/
export let toursData, filteredTours;

/**
 * Fetches the data from the provided endpoint and store the response.
 * @function
 */
export const fetchData = async () => {
    const response = await fetch('https://api.myjson.com/bins/6iv3y');
    toursData = await response.json();
}

/**
 * For each tour, finds and store the min and max prices.
 * @function
 */
export const setMinMaxPrices = _ => {
    const tours = filteredTours || toursData;
    for(let tour of tours){
        tour.minPrice = tour.dates && tour.dates.length > 0 ? tour.dates.reduce((min, p) => p.eur < min ? p.eur : min, tour.dates[0].eur || 0) : 0;
        tour.maxPrice = tour.dates && tour.dates.length > 0 ? tour.dates.reduce((max, p) => p.eur > max ? p.eur : max, tour.dates[0].eur || 0) : 0;
    }
}

/**
 * Finds the tour's maxDiscount and return the discount formatted.
 * @function
 * @param {Array} dates - Array of tour's dates.
 * @return {string} Fromatted discount
 */
export const getMaxDiscount = dates => {
    const reducePerc = dates.reduce((max, p) => p.discount > max ? p.discount : max, dates[0].discount);
    return  reducePerc ? `<div class='tour-infos__discount'><p>${reducePerc}</p></div>` : '';
}

/**
 * Filter the tour's dates by the provided date onward.
 * @function
 * @param {string} minDate - Date to fiter the tour's date by.
 */
export const filterDates = minDate => {
    let dates = [];
    filteredTours = [];
    for (let tour of toursData) {
        let filteredTour = { ...tour};
        filteredTour.dates = filteredTour.dates.filter(date => date.start >= minDate);
        if (filteredTour.dates.length > 0) {
            filteredTours.push(filteredTour);
        }
    }
}

/**
 * Sort the tours by the sorter criteria..
 * @function
 * @param {string} sorter - Sorting criteria.
 */
export const sortTours = sorter => {
    const tours = filteredTours || toursData;
    switch (sorter) {
        case 'lowerPrice':
            tours.sort((obj1, obj2) => {
                return obj1.minPrice - obj2.minPrice;
            });
            break;
        case 'highestPrice':
            tours.sort((obj1, obj2) => {
                return obj2.maxPrice - obj1.maxPrice;
            });
            break;
        case 'shortestTour':
            tours.sort((obj1, obj2) => {
                return obj1.length - obj2.length;
            });
            break;
        case 'longestTour':
            tours.sort((obj1, obj2) => {
                return obj2.length - obj1.length;
            });
            break;
    }
}

/**
 * This function assumes that start and end destinations is the duplicate
 * city in the array.
 * @function
 * @param {Array} cities - Array of cities.
 * @return {string} The name of the city or a fallback value.
 */
export const getStartEndDestination = cities => {
    const citiesNames = cities.map(city => city.name);
    const duplicateCities = citiesNames.filter((item, index) => citiesNames.indexOf(item) != index);

    return duplicateCities[0] ? `${duplicateCities[0]} / ${duplicateCities[0]}` : 'n.a.';
}

/**
 * Extracts and format the availablities within the tour's dates array.
 * @function
 * @param {Array} dates - Array of dates.
 * @return {string} Formatted availabilities.
 */
export const drawAvailabilities = dates => {
    return dates.map(date => date.availability < 1 ? '' :
                            `<div class="tour-offer__space">
                                <div>${moment(date.start).format('D MMM Y')}</div>
                                <div>${date.availability <= 10 ? date.availability :  '10+'}  spaces left</div>
                            </div>`).join('');
}
/**
 * Flter the tour's dates by spaces availability.
 * @function
 * @param {Array} dates - Array of dates.
 * @return {Array} Filtered Array of dates.
 */
export const hasTourSpacesLeft = dates => {
    return dates.filter(date => date.availability > 0).length > 0
}
/**
 * Draw the stars rating based on the decimal rating provided as param.
 * @function
 * @param {number} rating - Represents the rating.
 * @return {string} Formatted rating.
 */
export const drawStars = rating => {
    return `<span class="orange fa ${ rating >= 1 ? `fa-star` : rating > 0 ?  `fa-star-half-o` : `fa-star-o`}"></span>
            <span class="orange fa ${ rating >= 2 ? `fa-star` : rating > 1 ?  `fa-star-half-o` : `fa-star-o`}"></span>
            <span class="orange fa ${ rating >= 3 ? `fa-star` : rating > 2 ?  `fa-star-half-o` : `fa-star-o`}"></span>
            <span class="orange fa ${ rating >= 4 ? `fa-star` : rating > 3 ?  `fa-star-half-o` : `fa-star-o`}"></span>
            <span class="orange fa ${ rating == 5 ? `fa-star` : rating > 4 ?  `fa-star-half-o` : `fa-star-o`}"></span>`;
}
