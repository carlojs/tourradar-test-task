# TourRadar Test Task / Frontend
Basic responsive search page fetching data from the TourRadar API and showing the result as vertical card layout.
Local data can be sorted and filtered by date as well.

## Installation
npm install

## Build
webpack

## Run WebServer (app available at http://localhost:8080)
webpack-dev-server

## jsDoc
./node_modules/.bin/jsdoc src/
